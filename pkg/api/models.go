package api

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

// Article represents a returned article object
type Article struct {
	ID            interface{} `json:"id"`
	WebsiteID     interface{} `json:"websiteID"`
	WebsiteURL    interface{} `json:"websiteURL"`
	WebsiteName   interface{} `json:"websiteName"`
	Identifier    interface{} `json:"identifier"`
	URL           interface{} `json:"URL"`
	Title         interface{} `json:"title"`
	Author        interface{} `json:"author"`
	PublishedDate interface{} `json:"publishedDate"`
	Claim         interface{} `json:"claim"`
	ClaimDate     interface{} `json:"claimDate"`
	Rating        interface{} `json:"rating"`
	RatingWebsite interface{} `json:"ratingWebsite"`
	Sources       []string    `json:"sources"`
	Tags          []string    `json:"tags"`
}

// FetchArticles will get all the articles from the DB or the articles matching a query
func FetchArticles(ctx context.Context, db *pgxpool.Pool, qry Query) <-chan *Article {
	articles := make(chan *Article)
	go func() {
		defer close(articles)
		args := make([]interface{}, 0)
		query :=
			`SELECT
				id, website_id, website_url, website_name,
				identifier, url, title, author, published_date,
				claim, claim_date, rating, sources, rating_website, tags
			FROM
				articles_view`
		if qry.Keyword != "" {
			args = append(args, qry.Keyword)
			query += fmt.Sprintf(` WHERE
				to_tsvector('english', claim) @@ websearch_to_tsquery($%d)`,
				len(args))
		}

		query += " ORDER BY published_date desc"

		if qry.PageSize > 0 {
			args = append(args, qry.PageSize)
			query += fmt.Sprintf(" LIMIT $%d", len(args))
			if qry.PageNO > 1 {
				offset := (qry.PageNO - 1) * qry.PageSize
				args = append(args, offset)
				query += fmt.Sprintf(" OFFSET $%d", len(args))
			}
		}

		rows, err := db.Query(ctx, query, args...)
		if err != nil {
			log.Printf("Error found fetching rows for articles: %+v", errors.Wrap(err, "Fetch Articles Error"))
		}
		for rows.Next() {
			var art Article
			rows.Scan(
				&art.ID, &art.WebsiteID, &art.WebsiteURL,
				&art.WebsiteName, &art.Identifier, &art.URL,
				&art.Title, &art.Author, &art.PublishedDate,
				&art.Claim, &art.ClaimDate, &art.Rating, &art.Sources,
				&art.RatingWebsite, &art.Tags)
			select {
			case <-ctx.Done():
				rows.Close()
				return
			case articles <- &art:
			}
		}

		err = rows.Err()
		if err != nil {
			log.Printf("Error found fetching rows for articles: %+v", errors.Wrap(err, "Fetch Articles Error"))
		}
	}()
	return articles
}

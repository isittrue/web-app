package api

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"sync"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/julienschmidt/httprouter"
)

// Query represents a new API query
type Query struct {
	Keyword  string `json:"keyword"`
	PageNO   uint   `json:"pageNo"`
	PageSize uint   `json:"pageSize"`
}

// Response represents the API response
type Response struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Query   Query       `json:"query"`
}

func SearchHandler(ctx context.Context, db *pgxpool.Pool) httprouter.Handle {
	getArticles := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		values := r.URL.Query()

		getNo := func(p string) (n int) {
			v := values.Get(p)
			if v != "" {
				n, _ = strconv.Atoi(v)
			}
			return n
		}

		pageNo := uint(getNo("p"))
		pageSize := uint(getNo("ps"))

		if pageNo == 0 {
			pageNo = 1
		}
		if pageSize == 0 || pageSize > 50 {
			pageSize = 50
		}

		query := Query{
			Keyword:  values.Get("q"),
			PageNO:   pageNo,
			PageSize: pageSize,
		}

		articles := FetchArticles(ctx, db, query)

		data := make([]*Article, 0)

		var wg sync.WaitGroup

		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				select {
				case <-ctx.Done():
					return
				case article, ok := <-articles:
					if !ok {
						return
					}
					data = append(data, article)
				}
			}
		}()
		wg.Wait()

		w.Header().Set("Content-Type", "application/json")

		response := Response{
			Status:  "success",
			Message: "All OK!",
			Data:    data,
			Query:   query,
		}

		body, err := json.Marshal(response)
		if err != nil {
			w.WriteHeader(500)
			body = []byte(`{"error": "unexpected error found"}`)
		}
		w.Write(body)
	}

	return getArticles
}

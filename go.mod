module gitlab.com/isittrue/web-app

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/jackc/pgx/v4 v4.11.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/pkg/errors v0.8.1
)

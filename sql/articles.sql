-- Articles Query View

CREATE OR REPLACE VIEW articles_view AS
    SELECT art.id as "id",
           art.website_id as "website_id",
           ws.url as "website_url",
           ws.name as "website_name",
           art.identifier as "identifier",
           art.url as "url",
           art.title as "title",
           art.author as "author",
           to_char(art.published_date, 'YYYY-mm-dd') as "published_date",
           art.claim as "claim",
           to_char(claim_date, 'YYYY-mm-dd') as "claim_date",
           art.rating as "rating",
           art.sources as "sources",
           art.metadata->'rating' as "rating_website",
           (SELECT array(SELECT t.name FROM tags t
		        WHERE t.id IN (SELECT at.tag_id FROM articles_tags at WHERE
			                    at.article_id = art.id))) as "tags"
    FROM articles art JOIN websites ws ON (art.website_id = ws.id);

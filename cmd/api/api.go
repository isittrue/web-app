package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/BurntSushi/toml"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/isittrue/web-app/pkg/api"
)

func getDatabaseConfig(filename string) (config map[string]map[string]string) {
	cfile, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	toml.Unmarshal(cfile, &config)
	return config
}

func main() {
	var port uint
	var develop bool
	var dbconfig string

	flag.UintVar(&port, "port", 8080, "The port the server will listen")
	flag.BoolVar(&develop, "develop", false, "If enabled then the static website will be served")
	flag.StringVar(&dbconfig, "dbconfig", "database.toml", "Filename of the toml file with the database configuration")
	flag.Parse()

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	configDatabase := getDatabaseConfig(dbconfig)

	pgdb, err := pgxpool.Connect(ctx, configDatabase["postgres"]["uri"])
	if err != nil {
		log.Fatal("Unable to connect to PostgreSQL")
	}
	defer pgdb.Close()

	router := httprouter.New()
	if develop {
		fmt.Println("Website enabled in /website")
		router.ServeFiles("/website/*filepath", http.Dir("./website"))
		fmt.Println("Exports enabled in /export")
		router.ServeFiles("/export/*filepath", http.Dir("./export"))
	}
	router.GET("/api/search/", api.SearchHandler(ctx, pgdb))

	server := &http.Server{Addr: fmt.Sprintf(":%d", port), Handler: router}

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("Listening on port: ", port)
		server.ListenAndServe()
	}()

	<-ctx.Done()
	server.Shutdown(ctx)

	wg.Wait()

}

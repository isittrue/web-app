package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/BurntSushi/toml"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"gitlab.com/isittrue/web-app/pkg/api"
)

func getDatabaseConfig(filename string) (config map[string]map[string]string) {
	cfile, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	toml.Unmarshal(cfile, &config)
	return config
}

func exportToJSON(ctx context.Context, file io.Writer, articles <-chan *api.Article) <-chan *api.Article {
	output := make(chan *api.Article)
	go func() {
		defer close(output)
		for {
			select {
			case <-ctx.Done():
				return
			case article, ok := <-articles:
				if !ok {
					return
				}
				jsonl, err := json.Marshal(article)
				if err != nil {
					log.Printf("Export to JSON error: %+v", errors.Wrap(err, "JSON Exporter Error"))
				}
				_, err = file.Write(jsonl)
				file.Write([]byte("\n"))

				select {
				case <-ctx.Done():
					return
				case output <- article:
				}
			}
		}
	}()
	return output
}

func exportToCSV(ctx context.Context, file io.Writer, articles <-chan *api.Article) {
	writer := csv.NewWriter(file)
	header := []string{
		"id", "website_id", "website_url", "website_name", "identifier",
		"url", "title", "author", "published_date", "claim",
		"claim_date", "rating", "rating_website", "sources", "tags"}
	err := writer.Write(header)
	if err != nil {
		log.Printf("Export to CSV error: %+v", errors.Wrap(err, "CSV Exporter Error"))
		return
	}

	appendNilString := func(row *[]string, val interface{}) {
		if val != nil {
			*row = append(*row, val.(string))
		} else {
			*row = append(*row, "")
		}
	}

	bufferSize := 100

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case article, ok := <-articles:
			if !ok {
				break loop
			}
			row := make([]string, 0)
			row = append(row, strconv.Itoa(int(article.ID.(int32))))
			row = append(row, strconv.Itoa(int(article.WebsiteID.(int32))))
			row = append(row, article.WebsiteURL.(string))
			row = append(row, article.WebsiteName.(string))
			row = append(row, article.Identifier.(string))
			row = append(row, article.URL.(string))
			row = append(row, article.Title.(string))
			appendNilString(&row, article.Author)
			row = append(row, article.PublishedDate.(string))
			row = append(row, article.Claim.(string))
			appendNilString(&row, article.ClaimDate)
			row = append(row, article.Rating.(string))
			appendNilString(&row, article.RatingWebsite)
			row = append(row, strings.Join(article.Sources, ", "))
			row = append(row, strings.Join(article.Tags, ", "))
			err := writer.Write(row)
			if err != nil {
				log.Printf("Export to CSV error: %+v", errors.Wrap(err, "CSV Exporter Error"))
				continue loop
			}
			bufferSize -= 1
			if bufferSize <= 0 {
				writer.Flush()
				bufferSize = 100
			}
		}
	}

	writer.Flush()

	if err := writer.Error(); err != nil {
		log.Printf("Export to CSV error: %+v", errors.Wrap(err, "CSV Exporter Error"))
	}
}

func main() {
	var dbconfig string
	var jsonpath string
	var csvpath string

	flag.StringVar(&dbconfig, "dbconfig", "database.toml", "Filename of the toml file with the database configuration")
	flag.StringVar(&jsonpath, "json", "./export/articles.json", "Path to the JSON file to export")
	flag.StringVar(&csvpath, "csv", "./export/articles.csv", "Path to the CSV file to export")
	flag.Parse()

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	configDatabase := getDatabaseConfig(dbconfig)

	pgdb, err := pgxpool.Connect(ctx, configDatabase["postgres"]["uri"])
	if err != nil {
		log.Fatal("Unable to connect to PostgreSQL")
	}
	defer pgdb.Close()

	jsonfile, err := os.Create(jsonpath)
	defer jsonfile.Close()

	if err != nil {
		log.Fatalln("failed to open file", err)
	}

	csvfile, err := os.Create(csvpath)
	defer csvfile.Close()

	if err != nil {
		log.Fatalln("failed to open file", err)
	}

	articles := exportToJSON(ctx, jsonfile, api.FetchArticles(ctx, pgdb, api.Query{}))
	exportToCSV(ctx, csvfile, articles)
}
